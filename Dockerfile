FROM ubuntu:bionic

ARG GO_VERSION=1.13.3
ARG RUBY_VERSION=2.7.2
ARG RUBY_BUNDLER_VERSION=2.1.4
# ARG RUBY_BUNDLER_VERSION=1.17.3
ARG GIT_VERSION=2.29.0
ARG NODE_VERSION=12
ARG IDENTIFIER_PATH_IN_CONTAINER=/thin-gdk-in-container
ARG KIT_ROOT_IN_CONTAINER=/thin-gdk
ARG CHROMEDRIVER_VERSION=86.0.4240.22

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && \
    apt-get install -y parallel wget curl libreadline-dev zlib1g-dev \
        build-essential redis-server git libpq-dev ed libicu-dev \
        cmake libmysqlclient-dev libsqlite3-dev libre2-dev lsb-core iproute2 net-tools \
        iputils-ping openssh-server

RUN apt-get install -y tzdata libpam-krb5 libkrb5-dev

# Install Docker
RUN apt-get update && \
    apt-get install -y apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
RUN curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
RUN add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
RUN apt-get update && \
    apt-get install -y docker-ce docker-ce-cli containerd.io

# Install Git
RUN apt-get install -y libssl-dev libghc-zlib-dev libcurl4-gnutls-dev libexpat1-dev gettext
RUN cd /usr/src/
RUN wget https://github.com/git/git/archive/v${GIT_VERSION}.tar.gz -O git.tar.gz
RUN tar -xf git.tar.gz
RUN cd git-* && \
    make prefix=/usr/local all && \
    make prefix=/usr/local install

# Install GoLang
RUN cd tmp/
RUN wget https://dl.google.com/go/go${GO_VERSION}.linux-amd64.tar.gz
RUN tar -xvf go${GO_VERSION}.linux-amd64.tar.gz
RUN mv go /usr/local
RUN ln -s /usr/local/go/bin/* /usr/local/bin

# Install system ruby
RUN cd ~ && \
    wget https://cache.ruby-lang.org/pub/ruby/${RUBY_VERSION%.*}/ruby-${RUBY_VERSION}.tar.gz && \
    tar -xvzf ruby-${RUBY_VERSION}.tar.gz && \
    cd ruby-${RUBY_VERSION} && \
    ./configure && \
    make && \
    make install

# Install PostgreSQL
RUN sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt/ `lsb_release -cs`-pgdg main" >> /etc/apt/sources.list.d/pgdg.list'
RUN wget -q https://www.postgresql.org/media/keys/ACCC4CF8.asc -O - | apt-key add -
RUN apt-get update && apt-get install -y postgresql-11

# Install Yarn
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
RUN apt-get update && \
    apt-get install -y --no-install-recommends yarn

# Install Node
RUN curl -sL https://deb.nodesource.com/setup_${NODE_VERSION}.x | bash -
RUN apt-get update && \
    apt-get install -y --no-install-recommends nodejs

# Install chromedriver
ENV CHROMEDRIVER_DIR /chromedriver
RUN apt-get install -y wget xvfb unzip
RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
RUN echo "deb http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list
RUN apt-get update -y
RUN apt-get install -y google-chrome-stable
RUN mkdir $CHROMEDRIVER_DIR
RUN wget -q --continue -P $CHROMEDRIVER_DIR "http://chromedriver.storage.googleapis.com/$CHROMEDRIVER_VERSION/chromedriver_linux64.zip"
RUN unzip $CHROMEDRIVER_DIR/chromedriver* -d $CHROMEDRIVER_DIR

EXPOSE 8181

ENV ENABLE_SPRING=1
ENV PATH $CHROMEDRIVER_DIR:$PATH

COPY . ${KIT_ROOT_IN_CONTAINER}
WORKDIR ${KIT_ROOT_IN_CONTAINER}

# Create identifier that this VM is guest
RUN echo "true" > ${IDENTIFIER_PATH_IN_CONTAINER}
