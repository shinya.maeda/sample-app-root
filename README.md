Deprecated - To be removed

## Description

Connecting https://gitlab.com/shinya.maeda/sample-rails-app

## How To configure

```shell
devkitkat configure all
devkitkat start postgres
devkitkat seed rails
```

## How To start

```shell
devkitkat start all
```
